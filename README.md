# Introdução
Nesta seção mostraremos tudo sobre o Fluentd. 


# Instalação do Fluentd
### Instalação do Repositorio
```sh
helm3 repo add kiwigrid https://kiwigrid.github.io
```

### Export das Variáveis de Ambiente
```sh
export HOST="xx.xx.xx.xx"
export SCHEME=https
export PORT=443
```

### Comandos para a Instalação do Daemon
```sh
helm3 install fluentd-daemon kiwigrid/fluentd-elasticsearch \
   --set elasticsearch.port=''{$PORT}'' \
   --set elasticsearch.scheme=''{$SCHEME}'' \
   --set elasticsearch.hosts=''{$HOST}'' \
   --namespace kube-logging
```

***

# EFK (Elasticsearch Fluentd e Kibana  #

Esse Repositorio consiste na instalação da stack EFK com propósito de enviar todos os logs do ambiente de infraestrutura ao Elasticsearch

### Consultar Indices via Browser ###
* http://host/_cat/indices

### Criando um Índice via terminal ###
* curl -X PUT -u user:password http://0.0.0.0/testing/_doc/1?pretty -H 'Content-Type: application/json' -d' {"description" : "teste","timestamp" : "2021-11-04T02:49:49" }' -k

### Deletando um Índice ###
* curl -X DELETE -u root:password https://<host>/<index> -k